#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
    programme de collecte et affichage d'infos meteo
"""
from logging.config import dictConfig
from src import MY_WEB_SERVER
from src.views.meteo_logdict import conf_dict

# logging setup
CONF = conf_dict('meteo_server.log')
dictConfig(CONF)

METEO_HOST = "0.0.0.0"
METEO_PORT = 5000
MY_WEB_SERVER.run(host=METEO_HOST, port=METEO_PORT, debug=True)
