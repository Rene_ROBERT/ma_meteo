# Meteo server

## Overview

This program collect and display meteo information

## Requirements

## configuration file

Configuration file is config_default.yaml

located in meteo/src/app_conf

It contains mainly information about

* loglevel
* proxy to get access to data
* mongodb API information

## Running via python3

To run the server, please execute the following:

```bash
pip3 install -r requirements.txt
cd src
python3 meteo.py
```

## Running with Docker

To run the server on a Docker container, please execute the following:

```bash
# building the image
docker build -t meteo_server .

# starting up the container
# need /data repository on the hosts
docker run -p 5000:5000 --volume=/data:/data meteo_server
```

## Use

open your browser to here:

```bash
http://localhost:5000/meteo
```
