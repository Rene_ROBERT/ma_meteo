#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""setup module
"""

from setuptools import setup, find_packages


setup(packages=find_packages('src/views'),
      package_dir={'': 'src/views'})
