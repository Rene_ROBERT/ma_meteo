#!/usr/bin/env python3
# coding: utf-8
"""
    programme de collecte et affichage d'infos meteo
"""
from flask import Flask
from src.views import home

MY_WEB_SERVER = Flask(__name__)
MY_WEB_SERVER.register_blueprint(home.MOD)
