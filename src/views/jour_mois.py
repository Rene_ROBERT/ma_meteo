#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
    jour et mois
"""


import datetime


def joursem(val):
    """joursem(): donne le jour de la semaine du jour courant"""
    now = datetime.datetime.today()
    if now.weekday()+val > 6:
        val = val - 7
    return ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi',
            'Samedi', 'Dimanche')[now.weekday()+val]


def mois_francais(val):
    """donne le mois en Francais"""
    return ('Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre',
            'Decembre')[val-1]
