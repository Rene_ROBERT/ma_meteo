#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
    choix icone
"""
import logging

LOGGER = logging.getLogger('meteo_server')
if LOGGER.hasHandlers():
    # Logger is already configured, remove all handlers
    LOGGER.handlers = []


def choix_icone(code):
    """choix d'icone en fonction du code recu"""
    LOGGER.debug('code icon recu : %s ', code)
    if code == 800:
        icon_file_name = "icon-2.svg"
    elif code == 801:
        icon_file_name = "icon-3.svg"
    elif code == 802:
        icon_file_name = "icon-5.svg"
    elif code in (803, 804):
        icon_file_name = "icon-6.svg"
    elif code == 701:
        icon_file_name = "icon-8.svg"
    elif code in (500, 501):
        icon_file_name = "icon-9.svg"
    elif code in (501, 502, 503, 504):
        icon_file_name = "icon-10.svg"
    elif code in (200, 201, 202):
        icon_file_name = "icon-11.svg"
    elif 210 <= code <= 232:
        icon_file_name = "icon-12.svg"
    elif code in (500, 600):
        icon_file_name = "icon-13.svg"
    elif code in (500, 601):
        icon_file_name = "icon-14.svg"
    else:
        icon_file_name = "unknown"
    LOGGER.debug('icon_file_name : %s ', icon_file_name)
    return icon_file_name
