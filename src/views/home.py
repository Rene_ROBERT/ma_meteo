#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
    ecran meteo
"""
import logging
from datetime import datetime, timedelta
from flask import Blueprint, render_template
# from meteo_utils import get_config
from meteo_client_to_api import (get_meteo_from_openweathermap,
                                 get_coming_days_from_openweathermap,
                                 get_maree_from_shom,
                                 get_infos_from_METAR,
                                 get_probe_infos_from_domoticz)
from jour_mois import joursem, mois_francais
from meteo_functions import (process_data_coming_days_openweathermap,
                             get_tide_high)
from meteo_utils import get_config
import meteo_exceptions
MOD = Blueprint('home', __name__, url_prefix='/')

LOGGER = logging.getLogger('meteo_server')
if LOGGER.hasHandlers():
    # Logger is already configured, remove all handlers
    LOGGER.handlers = []

id_probe_DHT22_exterieur = get_config("domoticz.id_probe_DHT22_exterieur")
id_probe_DHT22_interieur = get_config("domoticz.id_probe_DHT22_interieur")

@MOD.route(
    '/meteo',
    methods=['GET'])
def start():
    """
    menu principal
    GET = affichage de la page d'accueil
    """
    # location_id = get_config("openweathermap.location_id")
    location_id = 6429109
    try:
        infos = get_meteo_from_openweathermap(location_id)
    except meteo_exceptions.OpenWeatherMapException as error:
        LOGGER.error("problem : %s", error.args[0])
        return render_template('exception.j2',
                               message=error.args[0])
    try:
        infos4 = get_coming_days_from_openweathermap(location_id)
    except meteo_exceptions.OpenWeatherMapException as error:
        LOGGER.error("problem : %s", error.args[0])
        return render_template('exception.j2',
                               message=error.args[0])
    ressenti = "?"
    print(infos["main"])
    if "feels_like" in infos["main"]:
        ressenti = "{0:.1f}".format(infos["main"]["feels_like"])
    phrase = infos["weather"][0]["description"]
    date = datetime.now()
    current_date = str(date)[0:10]
    try:
        infos_maree = get_maree_from_shom(current_date)
    except meteo_exceptions.ShomException as error:
        LOGGER.error("problem : %s", error.args[0])
        message = {"problem": error.args[0]}
        return render_template('exception.j2',
                               message=message)
    try:
        infos_metar = get_infos_from_METAR()
    except meteo_exceptions.MetarException as error:
        LOGGER.error("problem : %s", error.args[0])
        message = {"problem": error.args[0]}
        return render_template('exception.j2',
                               message=message)
    try:
        infos_probe_interieur = get_probe_infos_from_domoticz(str(id_probe_DHT22_interieur))
    except meteo_exceptions.DomoticzException as error:
        LOGGER.error("problem : %s", error.args[0])
        message = {"problem": error.args[0]}
        return render_template('exception.j2',
                               message=message)
    try:
        infos_probe_exterieur = get_probe_infos_from_domoticz(str(id_probe_DHT22_exterieur))
    except meteo_exceptions.DomoticzException as error:
        LOGGER.error("problem : %s", error.args[0])
        message = {"problem": error.args[0]}
        return render_template('exception.j2',
                               message=message)
    threshold_time = datetime.now() - timedelta(hours=1)
    # last_probe_update_string= infos_probe_exterieur['result'][0]['LastUpdate']
    # last_probe_update_time = datetime.strptime(last_probe_update_string, '%Y-%m-%d %H:%M:%S')
    # print("last_probe_update_string : ", last_probe_update_string)
    # print("last_probe_update_time : ",last_probe_update_time)
    # print("threshold_time : ", threshold_time)
    # print("infos_probe_exterieur-BMP :", infos_probe_exterieur_bmp)
    #if last_probe_update_time < threshold_time:
        #dev = finddev(idVendor=0x1a86, idProduct=0x7523)
        #dev.reset()
        # message = "Problème: derniere collecte de température le " + last_probe_update_string
        # return render_template('exception.j2', message=message)
    high_tides = get_tide_high(infos_maree, current_date)
    forecast_current_day, data_coming_days = process_data_coming_days_openweathermap(infos4,
                                                                                     infos_maree)
    first_tide_level = high_tides[0]
    second_tide_level = high_tides[2]
    time_first_tide = high_tides[1]
    time_second_tide = high_tides[3]
    tmp1 = infos['main']['temp']
    icone = infos['weather'][0]['icon']
    current_hour = date.hour
    now = datetime.now()
    current_minute = now.strftime("%M")
    real_temp_interieur = infos_probe_interieur['result'][0]['Temp']
    real_temp_exterieur = infos_probe_exterieur['result'][0]['Temp']
    #temp_exterieur = infos_metar.temp.string()
    barometre_exterieur = infos_metar.press.string()
    #barometre_exterieur = int(infos_probe_exterieur_bmp['result'][0]['Barometer'])
    real_humidity_exterieur = infos_probe_exterieur['result'][0]['Humidity']
    #real_humidity_exterieur = "--"
    return render_template('home.j2',
                           temp=f'{0:.1f}{tmp1}',
                           ressenti=ressenti,
                           phrase=phrase,
                           #pressure=infos['main']['pressure'],
                           pressure=barometre_exterieur,
                           temp_min=infos['main']['temp_min'],
                           temp_max=infos['main']['temp_max'],
                           #humidity=infos['main']['humidity'],
                           humidity=real_humidity_exterieur,
                           day_current=joursem(0),
                           day_number=date.today().day,
                           month=mois_francais(date.today().month),
                           icone=icone,
                           forecast_current_day=forecast_current_day,
                           data_coming_days=data_coming_days,
                           current_date=current_date,
                           current_hour=current_hour,
                           current_minute=current_minute,
                           first_tide_level=first_tide_level,
                           second_tide_level=second_tide_level,
                           time_first_tide=time_first_tide,
                           time_second_tide=time_second_tide,
                           real_temp_interieur=real_temp_interieur,
                           real_temp_exterieur=real_temp_exterieur
                           )
