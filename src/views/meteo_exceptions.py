#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

""" Module to define testing exceptions """

__author__ = ("Rene Robert <rene.jacob-robert@orange.fr>")


class DomoticzException(Exception):
    """Problem with domoticz communication"""


class MongodbException(Exception):
    """Problem with mongodb communication"""


class OpenWeatherMapException(Exception):
    """Communication problem with OpenWeatherMap api"""


class LocationNotFoundException(Exception):
    """LocationId not found in OpenWeatherMap"""

class AccesRightException(Exception):
    """Problème de droit d'accès à Domoticz"""

class InfoclimatException(Exception):
    """Communication problem with Infoclimat api"""


class ShomException(Exception):
    """Communication problem with Shom api"""


class MetarException(Exception):
    """Communication problem with METAR api"""


class WeatherException(Exception):
    """Communication problem with Weather api"""
