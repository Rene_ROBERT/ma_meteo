#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""clients to other API
"""
import logging
import requests
import linecache
from metar import Metar
import meteo_exceptions as meteo_excep
from meteo_utils import get_config

OPENWEATHERMAP_API_BASE_URL = (get_config("openweathermap.host") +
                               get_config("openweathermap.url"))
OPENWEATHERMAP_API_BASE_URL2 = (get_config("openweathermap.host") +
                                get_config("openweathermap.url2"))
OPENWEATHERMAP_API_HEADER = get_config("openweathermap.headers")
OPENWEATHERMAP_API_KEY = get_config("openweathermap.api_key")

SHOM_API_BASE_URL = (get_config("shom.host") +
                     get_config("shom.url"))
SHOM_API_HEADER = get_config("shom.headers")

METAR_API_BASE_URL = (get_config("METAR.host") +
                     get_config("METAR.url"))
METAR_API_HEADER = get_config("METAR.headers")

DOMOTICZ_API_BASE_URL = (get_config("domoticz.host") +
                         get_config("domoticz.url"))
DOMOTICZ_API_HEADER = get_config("domoticz.headers")

LOGGER = logging.getLogger('meteo_server')
if LOGGER.hasHandlers():
    # Logger is already configured, remove all handlers
    LOGGER.handlers = []


def get_probe_infos_from_domoticz(id_probe):
    """get probes infos from domoticz
    """
    LOGGER.debug('call to get_probe_infos_from_domoticz function')
    proxies = {}
    url = DOMOTICZ_API_BASE_URL + id_probe

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=DOMOTICZ_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with Domoticz"
        LOGGER.error(message)
        raise meteo_excep.DomoticzException(message)
    if response.status_code == 404:
        message = "not found in Domoticz"
        LOGGER.error(message)
        raise meteo_excep.LocationNotFoundException(message)
    if response.status_code == 401:
        message = "Problème de droit d'accès à Domoticz"
        LOGGER.error(message)
        raise meteo_excep.AccesRightException(message)
    response = response.json()
    print ("response = ",response)
    return response


def get_meteo_from_openweathermap(location_id):
    """get meteo for a location
    """
    LOGGER.debug('call to get_meteo_from_openweathermap function')
    proxies = {}
    url = (OPENWEATHERMAP_API_BASE_URL +
           "?id=" +
           str(location_id) +
           "&units=metric" +
           "&appid=" +
           str(OPENWEATHERMAP_API_KEY) +
           "&lang=fr")

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=OPENWEATHERMAP_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with OpenWeatherMap"
        LOGGER.error(message)
        raise meteo_excep.OpenWeatherMapException(message)
    if response.status_code == 404:
        message = "not found in OpenWeatherMap"
        LOGGER.error(message)
        raise meteo_excep.LocationNotFoundException(message)
    response = response.json()
    return response


def get_coming_days_from_openweathermap(location_id):
    """get meteo for a location
    """
    LOGGER.debug('call to get_coming_days_from_openweathermap function')
    proxies = {}
    url = (OPENWEATHERMAP_API_BASE_URL2 +
           "?id=" +
           str(location_id) +
           "&units=metric" +
           "&appid=" +
           str(OPENWEATHERMAP_API_KEY) +
           "&lang=fr")

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=OPENWEATHERMAP_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with OpenWeatherMap"
        LOGGER.error(message)
        raise meteo_excep.OpenWeatherMapException(message)
    if response.status_code == 404:
        message = "not found in OpenWeatherMap"
        LOGGER.error(message)
        raise meteo_excep.LocationNotFoundException(message)
    response = response.json()
    LOGGER.debug('response : %s ', response)
    return response


def get_maree_from_shom(current_date):
    """get meteo info
    """
    LOGGER.debug('call to get_maree_from_shom function')
    proxies = {}
    url = SHOM_API_BASE_URL.replace("{{date}}", current_date)
    headers = SHOM_API_HEADER
    headers["Referer"] = headers["Referer"].replace("{{date}}", current_date)
    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    LOGGER.debug('headers used : %s ', headers)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=headers,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with SHOM site"
        LOGGER.error(message)
        raise meteo_excep.ShomException(message)
    response = response.json()
    return response


def get_infos_from_METAR():
    """get meteo info
    """
    LOGGER.debug('call to get_infos_from_METAR function')
    url = METAR_API_BASE_URL
    headers = SHOM_API_HEADER
    proxies = {}
    headers = {}
    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    LOGGER.debug('headers used : %s ', headers)
    try:
        response = requests.request("GET",
                                    url)
        metar_data = response.text
        print(metar_data)
        obs_debut = metar_data.find("LFRO") 
        obs_metar = Metar.Metar(metar_data[obs_debut:])
        print(obs_metar.temp.string())
    except Exception:
        message = "communication problem with METAR site"
        LOGGER.error(message)
        raise meteo_excep.MetarException(message)
    return obs_metar
