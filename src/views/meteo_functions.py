#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

"""
    some functions
"""
import logging
import json
from datetime import datetime, timedelta
from jour_mois import joursem

LOGGER = logging.getLogger('meteo_server')
if LOGGER.hasHandlers():
    # Logger is already configured, remove all handlers
    LOGGER.handlers = []


def get_tide_high(infos_maree, day):
    """
    get high tide infos
    """
    if infos_maree[day][0][0] == "tide.high":
        first_tide_level = infos_maree[day][0][3]
        time_first_tide = infos_maree[day][0][1]
        second_tide_level = infos_maree[day][2][3]
        time_second_tide = infos_maree[day][2][1]
    else:
        first_tide_level = infos_maree[day][1][3]
        time_first_tide = infos_maree[day][1][1]
        second_tide_level = infos_maree[day][3][3]
        time_second_tide = infos_maree[day][3][1]
    high_tides = [first_tide_level,
                  time_first_tide,
                  second_tide_level,
                  time_second_tide]
    return high_tides


def process_data_coming_days_openweathermap(infos, infos_maree):
    """
    calculate temperature for coming days
    """
    LOGGER.debug('call to process_data_coming_days_openweathermap function')
    heure_max = " 15:00:00"
    heure_min = " 06:00:00"
    forecast_current_day = {}
    data_coming_days = []
    date = datetime.now()
    for idx in range(7):
        data_coming_day = {}
        day = str(date + timedelta(days=idx))[0:10]
        if idx == 0:
            if date.hour < 15:
                for info in infos["list"]:
                    if day == str(info["dt_txt"])[0:10]:
                        if info["dt_txt"] == day + heure_max:
                            forecast_current_day["temp_max_day"] = info["main"]["temp"]
                            forecast_current_day["icone"] = info["weather"][0]["icon"]
                            forecast_current_day["description"] = info["weather"][0]["description"]                
        else:
            for info in infos["list"]:
                if day == str(info["dt_txt"])[0:10]:
                    if info["dt_txt"] == day + heure_min:
                        temp_min_day = info["main"]["temp"]
                    
                    if info["dt_txt"] == day + heure_max:
                        temp_max_day = info["main"]["temp"]
                        pression_day = info["main"]["pressure"]
                        ressenti = "?"
                        if "feels_like" in info["main"]:
                            ressenti = info["main"]["feels_like"]
                        icone = info["weather"][0]["icon"]
                        description = info["weather"][0]["description"]
            
            high_tides = get_tide_high(infos_maree, day)
            data_coming_day["day"] = joursem(idx)
            data_coming_day["temp_max_day"] = "{0:.1f}".format(temp_max_day)
            data_coming_day["temp_min_day"] = "{0:.1f}".format(temp_min_day)
            data_coming_day["pression_day"] = pression_day
            data_coming_day["ressenti"] = ressenti
            data_coming_day["icone"] = icone
            data_coming_day["description"] = description
            data_coming_day["first_tide_level"] = high_tides[0]
            data_coming_day["second_tide_level"] = high_tides[2]
            data_coming_day["time_first_tide"] = high_tides[1]
            data_coming_day["time_second_tide"] = high_tides[3]
            data_coming_days.append(data_coming_day)
        
    LOGGER.debug('data_coming_days : %s ', data_coming_days)
    return forecast_current_day, data_coming_days
